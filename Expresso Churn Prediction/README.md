# Expresso Churn ([ExC](https://zindi.africa/competitions/expresso-churn-prediction)) Prediction- Rosato Luca

## Preprocessing:

* One Hot Encoder on categorical variabile
* Extract statistics from features
* Grouping averaging on different categorical features

## Models:

* Implement lgbm part of staking with 20 folds
* Implement catboost part of staking with 20 folds

## commit:

* Weight average (lgbm_pred x 0.4 + catboost_pred x 0.6) between the predictions of the previous models 
* 4th best score with AUC=0.932188396006762
