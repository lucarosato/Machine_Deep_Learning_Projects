# Tourism Classification Challenge ([TC](https://zindi.africa/competitions/ai4d-lab-tanzania-tourism-classification-challenge)) Prediction - Rosato Luca

## Preprocessing:

* One Hot Encoder on categorical variabile
* Extract statistics from features
* Grouping averaging on different categorical features

## Models:

* Implement lgbm part of staking with 10 folds
* Implement catboost part of staking with 10 folds

## Stack Models:

* Weight average (lgbm_pred x 0.42 + catboost_pred x 0.58) between the predictions of the previous models
* Lgbm meta model(lgbm_pred + catboost_pred) between the predictions of the previous models
* Catboost meta model(lgbm_pred + catboost_pred) between the predictions of the previous models

## Submission:

* Average (meta_lgbm_pred + meta_catboost_pred + W_average_pred) / 3 between the predictions of the previous models
* 1st best score with Log Loss=1.032027519681248


