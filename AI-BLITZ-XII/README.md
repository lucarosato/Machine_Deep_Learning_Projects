# [AI-BLITZ-XII](https://www.aicrowd.com/challenges/ai-blitz-xii)


## Introduction

Language has been an integral part of Human Evolution. Allowing us to share our ideas, thoughts, and feelings with each other, language has enabled us to build societies. If you are reading this right now, it means you have mastered a complex system of words, structure, and grammar to effectively communicate with others. But what about the field that has been enabling our machines to do the same?

## puzzles AI Blitz in Repository

* [Programming Language Classification](https://www.aicrowd.com/challenges/ai-blitz-xii/problems/programming-language-classification): Classifying a corpus of various programming lines into 15 Programming Languages.
* [Speaker Identification](https://www.aicrowd.com/challenges/ai-blitz-xii/problems/speaker-identification): Perform clustering on a corpus of transcripts taken from Youtube videos by capturing voiceprints of the given data samples.
* [Language Translation](https://www.aicrowd.com/challenges/ai-blitz-xii/problems/language-translation): Perform Language translation from English to Crowd Talk, AIcrowd’s very own made-up language.

