from Updater import Updater
import os
import numpy as np
from PIL import Image
from bounding_box import bounding_box as bb
from matplotlib import pyplot as plt
from cv2 import imwrite

from typing_extensions import Concatenate
import ultralytics


model = ultralytics.YOLO('net_model/best.pt')
print('##### READY #####')

def imageHandler(bot, message, chat_id, local_filename):

    print(local_filename)
    
    bot.sendMessage(chat_id, "Hi, please wait ...")
    results = model.predict(local_filename)
    
    print(results[0].boxes)
    res_plotted = results[0].plot()

    path_final = local_filename.split('.')
    path_final = path_final[0] + '_res.' + path_final[1]
    imwrite(path_final, res_plotted)
    bot.sendImage(chat_id, path_final, "")

    os.remove(local_filename)
    os.remove(path_final)

if __name__ == "__main__":
    bot_id = ''  # api token
    updater = Updater(bot_id)
    updater.setPhotoHandler(imageHandler)
    updater.start()
