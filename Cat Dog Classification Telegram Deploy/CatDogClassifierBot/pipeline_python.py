from Updater import Updater
import torch
import torch.nn as nn
from sklearn.preprocessing import LabelEncoder
import numpy as np
from PIL import Image
from torchvision import models

from typing_extensions import Concatenate
import albumentations as A
from albumentations.pytorch.transforms import ToTensorV2


device = torch.device('cpu')

class Net(nn.Module):
    def __init__(self, name, weights=None, n_classes=1):
        super(Net, self).__init__()
        
        if name == 'efficientnet_v2_s':
            self.model = models.efficientnet_v2_s(weights=weights)
            self.model.classifier[1] = nn.Linear(self.model.classifier[1].in_features, n_classes)

    def forward(self, x):
        x = self.model(x)
        return x

le = LabelEncoder()
le.classes_ = np.load('net_model/classes_v2.npy')

model = Net('efficientnet_v2_s', weights=None, n_classes=len(le.classes_))
model.load_state_dict(torch.load('net_model/efficientnet_v2_s.pth', map_location=device))

print('##### READY #####')

def imageHandler(bot, message, chat_id, local_filename):

    print(local_filename)
    
    bot.sendMessage(chat_id, "Hi, please wait ...")
    dim = 384
    im = Image.open(local_filename)
    im = np.array(im.resize((dim, dim)))

    transform = A.Compose([
        A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225), max_pixel_value=255.0),
        ToTensorV2(p=1.0)
        ])
    im = transform(image=im)['image']
    
    model.eval()
    with torch.no_grad():
        pred = model(im.unsqueeze(0).to(device).float()).detach().softmax(1).cpu().numpy()
    
    zipped = zip(pred[0], le.classes_)
    res = sorted(zipped, key = lambda x: x[0], reverse=True) 
    pred = le.inverse_transform([pred.argmax()])[0]
    top5 = list(res)[:5]
    print(pred)
    print(top5)
    bot.sendMessage(
        chat_id,
        "Top 5 breed are:\n\n1° - "+ str(" ".join(top5[0][1].split("_")))+" - Prob. of "+str(round(top5[0][0]*100,2))+
        "%\n2° - "+ str(" ".join(top5[1][1].split("_")))+" - Prob. of "+str(round(top5[1][0]*100,2))+
        "%\n3° - "+ str(" ".join(top5[2][1].split("_")))+" - Prob. of "+str(round(top5[2][0]*100,2))+
        "%\n4° - "+ str(" ".join(top5[3][1].split("_")))+" - Prob. of "+str(round(top5[3][0]*100,2))+
        "%\n5° - "+ str(" ".join(top5[4][1].split("_")))+" - Prob. of "+str(round(top5[4][0]*100,2))+
        "%"
    )
    os.remove(local_filename)

if __name__ == "__main__":
    bot_id = ''  # api token
    updater = Updater(bot_id)
    updater.setPhotoHandler(imageHandler)
    updater.start()
