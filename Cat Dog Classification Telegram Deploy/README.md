# Telegram Bot: Pet Classification 

## Processing:

Model: efficientnet_v2_s

Augmentation: HorizontalFlip, VerticalFlip, Transpose, Rotate, RandomBrightnessContrast, GaussNoise

Image size: 384 x 384

Validation: Holdout (80 train - 20 test)


## Performance:

![image](img/performances_v2.JPG)


## Output:

![image](img/siamese_v2.JPG)

# Telegram Bot: Pet Detection

## Processing:

Model: [YOLOv8s](https://github.com/ultralytics/ultralytics)

Augmentation: included in YOLOv8s's model 

Image size: 640 x 640

Validation: Holdout (90 train - 10 test)


## Performance:

![image](img/yolo_perf.JPG)


## Output:

![image](img/bengala.JPG)


## Bot telegram setup:

* Open botfather: https://t.me/botfather
* Type **/newbot** to create a new bot and set the bot name (ex: CatDogClassifier) and the bot username (ex: PetsClassifierEffnetv2Bot)
* botfather sends you **API token** and **bot link**
* inside bot **press start** 


## Python bot setup:

* you just have to put the api token in "**pipeline_python.py**", "**Updater.py**" and "**Bot.py**" where the comment "**#api token**" appears
* open **CMD** in project forlder and type "**python .\pipeline_python.py**" to start sending data to bot


## Top Dependances:

* Python 3.8
* Albumentations 1.3.0
* Torchvision 0.15.1
* Torch 2.0.0

## Disclaimer:

The following disclaimer is intended to clarify the limitations of responsibility regarding the incorrect or inappropriate use of the content provided. By accessing or using the content, you agree to the terms outlined below:

* Content for informational purposes only: The content provided is for general informational purposes and should not be considered as professional advice or a substitute for seeking professional guidance. The information may not be accurate, complete, or up-to-date, and should not be solely relied upon.

* User's responsibility: The user acknowledges that they are solely responsible for the way they use and interpret the content. Any actions or decisions made based on the content are at the user's own risk.

* No guarantee of accuracy or validity: While efforts are made to provide reliable and accurate information, no guarantee is given regarding the accuracy, validity, or completeness of the content. The content may contain errors, omissions, or outdated information.

* Personal interpretation: The content may be open to interpretation and may not reflect the views, opinions, or intentions of the content provider or any affiliated individuals or organizations. Users should exercise their own judgment and discretion when relying on the content.

* No liability: The content provider, including its agents, employees, or representatives, shall not be held liable for any damages, losses, or injuries arising from the use or misuse of the content. This includes but is not limited to direct, indirect, incidental, or consequential damages.

* Third-party content and links: The content may contain links to third-party websites or incorporate information from third-party sources. These external resources are beyond the control of the content provider, and no responsibility is assumed for their content, accuracy, or availability.

* Changes to the content and disclaimer: The content provider reserves the right to modify, update, or remove the content or this disclaimer at any time without prior notice.

By accessing or using the content, you agree to release and hold harmless the content provider from any claims, demands, or actions arising out of or in connection with the use or misuse of the content. If you do not agree with any part of this disclaimer, refrain from accessing or using the content.
