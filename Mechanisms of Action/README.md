# Mechanisms of Action ([MoA](https://www.kaggle.com/c/lish-moa)) Prediction- Rosato Luca

## Preprocessing:

* PCA for feature extraction
* One Hot Encoder for categorical variabile
* Quantile transformation before input in NN
* Extract statistics from features

## pytorch-tabnet:

* Use pytorch tabnet implementation (https://github.com/dreamquark-ai/tabnet)
* Implement tabnet part of staking with 5 folds and 3 seeds

## pytorch-nn:

* Implement 2 layer Neural Network
* Implement NN part of staking with 5 folds and 3 seeds

## pytorch-resnet:

* Implement 2 layer Neural Network with residual connection
* Implement ResNN part of staking with 5 folds and 3 seeds

## commit:

* Implements the simple average between the predictions of the previous models
* Provides a public score of 0.01829
* Provides a private score of 0.01615
